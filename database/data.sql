-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 27-07-2017 a las 11:34:56
-- Versión del servidor: 5.6.14
-- Versión de PHP: 5.5.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `cryptoview`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `data`
--

CREATE TABLE IF NOT EXISTS `data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `market` varchar(200) NOT NULL,
  `change_` varchar(50) NOT NULL,
  `bid` varchar(100) NOT NULL,
  `ask` varchar(100) NOT NULL,
  `low` varchar(100) NOT NULL,
  `high` varchar(100) NOT NULL,
  `volume` varchar(100) NOT NULL,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Volcado de datos para la tabla `data`
--

INSERT INTO `data` (`id`, `market`, `change_`, `bid`, `ask`, `low`, `high`, `volume`, `update_time`) VALUES
(1, 'yobit.net', 'LTC_BTC', '0,01640000', '0,01643019', '0,01620000', '0,01697951', '21,78464433', '2017-07-27 09:34:21'),
(2, 'livecoin.net', 'LTC_BTC', '0,01624006', '0,01639000', '0,01623000', '0,01689999', '12127,93736589', '2017-07-27 09:34:16'),
(3, 'gdax.com', 'LTC_BTC', '', '', '', '', '', '2017-07-26 13:11:11'),
(5, 'bitfinex.com', 'LTC_BTC', '0,01623600', '0,01624100', '0,01618700', '0,01691500', '72259,56587787', '2017-07-27 09:34:21'),
(7, 'bittrex.com', 'LTC_BTC', '0,01621000', '0,01626000', '0,01618608', '0,01689000', '58231,09604726', '2017-07-27 09:34:21'),
(8, 'poloniex.com', 'LTC_BTC', '', '', '', '', '', '2017-07-26 13:11:11'),
(9, 'cryptopia.co.nz', 'LTC_BTC', '0,01635000', '0,01639000', '0,01629000', '0,01700000', '2598,97791311', '2017-07-27 09:34:20'),
(10, 'bitfinex.com', 'BTC_USD', '2555,00', '2556,00', '2404,00', '2580,00', '28028,85', '2017-07-27 09:34:20'),
(11, 'gdax.com', 'BTC_USD', '', '', '', '', '', '2017-07-26 13:11:11'),
(13, 'xbtce.net', 'BTC_USD', '2547,80', '2565,00', '2421,20', '2579,00', '99,36', '2017-07-27 09:34:20'),
(14, 'bitstamp.net', 'BTC_USD', '2553,96', '2558,06', '2539,91', '2562,63', '339,87', '2017-07-27 09:34:20'),
(15, 'okcoin.com', 'BTC_USD', '2640,35', '2645,00', '2506,76', '2680,00', '515,54', '2017-07-27 09:34:21'),
(17, 'okcoin.com', 'LTC_CNY', '2640,35', '2645,00', '2506,76', '2680,00', '515,54', '2017-07-27 09:34:21'),
(18, 'chbtc.com', 'LTC_CNY', '284,33', '284,35', '277,30', '289,10', '64382,57', '2017-07-27 09:34:21'),
(19, 'bithumb.com', 'LTC_KRW', '46990,00', '47040,00', '46510,00', '47810,00', '170577,47', '2017-07-27 09:34:21'),
(20, 'bithumb.com', 'KRW_BTC', '2892000,00', '2893000,00', '2790000,00', '2921000,00', '13781,28', '2017-07-27 09:34:19'),
(21, 'okcoin.com', 'CNY_BTC', '2640,35', '2645,00', '2506,76', '2680,00', '515,54', '2017-07-27 09:34:21'),
(22, 'poloniex.com', 'USD_BTC', '', '', '', '', '', '2017-07-26 13:11:11');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
