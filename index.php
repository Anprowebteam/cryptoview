<!DOCTYPE html>
<html>
<head>
	<title>Elmiyagui CryptoView</title>

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

	<link rel="stylesheet" type="text/css" href="assets/css/styles.css">
	<script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
</head>
<body>
	 <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand" href="?">
                    CryptoView
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <ul class="nav navbar-nav">
                    &nbsp;
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            Cryptomonedas <span class="caret"></span>
                        </a>

                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="?">
                                    Listado
                                </a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    <li><a href="?">Elmiyagui</a></li>
                </ul>
            </div>
        </div>
    </nav>

    <div class="container">
    	<div class="row">
    		<div class="col-md-12">
		        <div class="panel panel-default">
		          <div class="panel-heading">
		            <h3>Listado de cryptomonedas</h3>
		          </div>
		          <div class="panel-body">
		          	<div class="table-responsive">
		                <table class="table">
		                  <thead>
		                    <tr>
		                      <th>MERCADO</th>
                              <th>Compra</th>
                              <th>Venta</th>
		                      <th>Minimo</th> 
		                      <th>Maximo</th>
                              <th>Volumen</th>
                              <th>Act</th>
		                    </tr>
		                  </thead>
		                  <tbody id="cryptodatos">

		                  </tbody>
		                </table>
		          </div>
		        </div>
		    </div>
    	</div>
    </div>

    <!-- Todo el codigo ajax y demas -->
	<script type="text/javascript" src="assets/js/scripts.js"></script>
</body>
</html>