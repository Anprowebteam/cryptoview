
import json
import requests
import MySQLdb
import time
import datetime
from multiprocessing.dummy import Pool as ThreadPool 
import threading

        
class Database:

    host = '127.0.0.1'
    user = 'root'
    password = ''
    db = 'cryptoview'

    def __init__(self):
        self.connection = MySQLdb.connect(self.host, self.user, self.password, self.db)
        self.cursor = self.connection.cursor()
        self.connection.ping(True)

    def init(self, data):
        try: 
            self.cursor.execute("TRUNCATE TABLE data")
            self.cursor.execute("ALTER TABLE data AUTO_INCREMENT=1")
            self.connection.commit()
        except MySQLdb.Error as e:
            self.connection.rollback()
            print e
            print "Error borrando datos existentes"

        try: 
            query = "INSERT INTO data (id, market, change_, bid, ask, low, high, volume, update_time) VALUES (%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            self.cursor.executemany(query, data)
            time.sleep(0.5)
            self.connection.commit()
        except MySQLdb.Error as e:
            try:
                print self.cursor._last_executed
                print "MySQL Error InsertDB [%d]: %s" % (e.args[0], e.args[1])
                return None
            except IndexError:
                print "MySQL Error: InsertDB %s" % str(e)
                return None
            self.connection.rollback()

    def update(self, data):
        try: 
            query = "UPDATE data SET market=%s, bid=%s, ask=%s, low=%s, high=%s, volume=%s, update_time=%s WHERE id=%s"
            self.cursor.executemany(query, data)
            time.sleep(0.1)
            self.connection.commit()
        except MySQLdb.Error as e:
            try:
                print '*** Conexion caida!'
            except IndexError:
                print "MySQL Error UpdateDB : %s" % str(e)
                return None

    def query(self, query):
        cursor = self.connection.cursor( MySQLdb.cursors.DictCursor )
        cursor.execute(query)

        return cursor.fetchall()

    def __del__(self):
        self.connection.close()



class CrypTValues:
    markets = []

    # LTC to BTC
    markets.append([1,'LTC_BTC','yobit.net','https://yobit.net/api/3/ticker/ltc_btc'])
    markets.append([2,'LTC_BTC','livecoin.net','https://api.livecoin.net/exchange/ticker?currencyPair=LTC/BTC'])
    markets.append([3,'LTC_BTC','gdax.com','https://api.gdax.com/products/LTC-BTC/ticker','https://api.gdax.com/products/LTC-BTC/STATS'])
    markets.append([4,'LTC_BTC','hitbtc.com','https://api.hitbtc.com/api/1/public/LTCBTC/ticker'])
    markets.append([5,'LTC_BTC','bitfinex.com','https://api.bitfinex.com/v1/pubticker/LTCBTC'])
    #markets.append([6,'LTC_BTC','btc-e.com','https://btc-e.com/api/2/ltc_btc/ticker']) Error del servidor
    markets.append([7,'LTC_BTC','bittrex.com','https://bittrex.com/api/v1.1/public/getmarketsummary?market=btc-ltc'])
    markets.append([8,'LTC_BTC','poloniex.com','https://poloniex.com/public?command=returnOrderBook&currencyPair=BTC_LTC&depth=1','https://poloniex.com/public?command=returnTicker&currencyPair=BTC_LTC&depth=1'])
    markets.append([9,'LTC_BTC','cryptopia.co.nz','https://www.cryptopia.co.nz/api/GetMarket/LTC_BTC'])

    # BTC to USD
    markets.append([10,'BTC_USD','bitfinex.com','https://api.bitfinex.com/v1/pubticker/BTCUSD'])
    markets.append([11,'BTC_USD','gdax.com','https://api.gdax.com/products/BTC-USD/ticker','https://api.gdax.com/products/BTC-USD/STATS'])
    #markets.append([12,'BTC_USD','btc-e.com','https://btc-e.com/api/3/ticker/btc_usd'])
    markets.append([13,'BTC_USD','xbtce.net','https://cryptottlivewebapi.xbtce.net:8443/api/v1/public/ticker/BTCUSD'])
    markets.append([14,'BTC_USD','bitstamp.net','https://www.bitstamp.net/api/v2/ticker_hour/btcusd/'])
    markets.append([15,'BTC_USD','okcoin.com','https://www.okcoin.com/api/v1/ticker.do?symbol=btc_usd'])

    #LTC to CNY
    #markets.append([16,'LTC_CNY','huobi.com','https://api.huobi.com/apiv3/market/detail/merged?symbol=ltc'])
    markets.append([17,'LTC_CNY','okcoin.com','https://www.okcoin.com/api/v1/ticker.do?symbol=ltc_cny'])
    markets.append([18,'LTC_CNY','chbtc.com','http://api.chbtc.com/data/ltc/ticker'])

    #LTC to KRW
    markets.append([19,'LTC_KRW','bithumb.com','https://api.bithumb.com/public/ticker/ltc'])

    #MON to BTC
    markets.append([20,'KRW_BTC','bithumb.com','https://api.bithumb.com/public/ticker/BTC'])
    markets.append([21,'CNY_BTC','okcoin.com','https://www.okcoin.com/api/v1/ticker.do?symbol=BTC_cny'])
    markets.append([22,'USD_BTC','poloniex.com','https://poloniex.com/public?command=returnOrderBook&currencyPair=USDT_BTC&depth=1','https://poloniex.com/public?command=returnTicker&currencyPair=USDT_BTC&depth=1'])

    db = Database()

    datos = []
    dataBaseArray = []
    toSaveData = []

    # valores en dolares
    usd_btc_bid = 0
    usd_btc_ask = 0
    usd_btc_low = 0
    usd_btc_high = 0
    usd_btc_volume = 0

    # valores en CNY
    cny_btc_bid = 0
    cny_btc_ask = 0
    cny_btc_low = 0
    cny_btc_high = 0
    cny_btc_volume = 0

    # valores en KRW
    krw_btc_bid = 0
    krw_btc_ask = 0
    krw_btc_low = 0
    krw_btc_high = 0
    krw_btc_volume = 0

    def __init__(self,debug = False):
        self.debug = debug

    def updateAll(self):
        while True:
            self.db.update(self.toSaveData)
            self.toSaveData = []
            time.sleep(0.8)

    def iniMarkets(self):

        i = 0
        for market in self.markets:
            #id, market, change_, bid, ask, low, high, volume, update_time
            self.dataBaseArray.append([self.markets[i][0], self.markets[i][2], self.markets[i][1],
            '','','','','','2017-07-26 09:11:11'])
            i = i + 1

        self.db.init(self.dataBaseArray)
        print 'Inicializada la base de datos! \n'


    def getMarket(self, market):

        while True:

            start_time = time.time()

            try:
                # accediendo a los datos actualizados
                actualMarket = requests.get(market[3])
                actualMarket = json.loads(actualMarket.text)
            except requests.exceptions.RequestException as e:
                print e

            # auxiliar para algunos mercados
            # *********
            # *********

            try:
                if(market[2] == 'gdax.com'):
                    # accediendo a los datos actualizados
                    auxMarket = requests.get(market[4])
                    auxMarket = json.loads(auxMarket.text)
            except requests.exceptions.RequestException as e:
                print e

            try:
                # auxiliar para algunos mercados
                if(market[2] == 'poloniex.com'):
                    # accediendo a los datos actualizados
                    auxMarket = requests.get(market[4])
                    auxMarket = json.loads(auxMarket.text)
            except requests.exceptions.RequestException as e:
                print e
            
            # *********
            # *********

            try:
                formatoMuestra = '%.08f'

                if(market[2] == 'hitbtc.com'):
                    Compra = str(formatoMuestra % float(actualMarket["bid"])).replace(".", ",")
                    Venta = str(formatoMuestra % float(actualMarket["ask"])).replace(".", ",")
                    Minimo = str(formatoMuestra % float(actualMarket["low"])).replace(".", ",")
                    Maximo = str(formatoMuestra % float(actualMarket["high"])).replace(".", ",")
                    Volumen = str(formatoMuestra % float(actualMarket["volume"])).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'poloniex.com' and market[0] == 8):
                    Compra = str(formatoMuestra % float(actualMarket["bids"][0][0])).replace(".", ",")
                    Venta = str(formatoMuestra % float(actualMarket["asks"][0][0])).replace(".", ",")
                    Minimo = str(formatoMuestra % float(auxMarket['BTC_LTC']['lowestAsk'])).replace(".", ",")
                    Maximo = str(formatoMuestra % float(auxMarket['BTC_LTC']['highestBid'])).replace(".", ",")
                    Volumen = str(formatoMuestra % float(auxMarket['BTC_LTC']['baseVolume'])).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'poloniex.com' and market[0] == 22):
                    # obtener los valores en USD
                    self.usd_btc_bid = float(actualMarket["bids"][0][0])
                    self.usd_btc_ask = float(actualMarket["asks"][0][0])
                    self.usd_btc_low = float(auxMarket['USDT_BTC']['lowestAsk'])
                    self.usd_btc_high = float(auxMarket['USDT_BTC']['highestBid'])
                    self.usd_btc_volume = float(auxMarket['USDT_BTC']['baseVolume'])

                    Compra = str(formatoMuestra % float(actualMarket["bids"][0][0])).replace(".", ",")
                    Venta = str(formatoMuestra % float(actualMarket["asks"][0][0])).replace(".", ",")
                    Minimo = str(formatoMuestra % float(auxMarket['USDT_BTC']['lowestAsk'])).replace(".", ",")
                    Maximo = str(formatoMuestra % float(auxMarket['USDT_BTC']['highestBid'])).replace(".", ",")
                    Volumen = str(formatoMuestra % float(auxMarket['USDT_BTC']['baseVolume'])).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'gdax.com'):

                    # valores en BTC
                    marketBid = float(actualMarket["bid"])
                    marketAsk = float(actualMarket["ask"])
                    marketLow = float(auxMarket["low"])
                    marketHigh = float(auxMarket["high"])
                    marketVolume = float(actualMarket["volume"])

                    if(market[0] == 11):
                        # valores en BTC
                        marketBid = marketBid / self.usd_btc_bid
                        marketAsk = marketAsk / self.usd_btc_ask
                        marketLow = marketLow / self.usd_btc_low
                        marketHigh = marketHigh / self.usd_btc_high
                        marketVolume = marketVolume / self.usd_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                
                if(market[2] == 'bittrex.com'):
                    Compra = str(formatoMuestra % actualMarket["result"][0]["Bid"]).replace(".", ",")
                    Venta = str(formatoMuestra % actualMarket["result"][0]["Ask"]).replace(".", ",")
                    Minimo = str(formatoMuestra % actualMarket["result"][0]["Low"]).replace(".", ",")
                    Maximo = str(formatoMuestra % actualMarket["result"][0]["High"]).replace(".", ",")
                    Volumen = str(formatoMuestra % actualMarket["result"][0]["Volume"]).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'cryptopia.co.nz'):
                    Compra = str(formatoMuestra % actualMarket["Data"]["BidPrice"]).replace(".", ",")
                    Venta = str(formatoMuestra % actualMarket["Data"]["AskPrice"]).replace(".", ",")
                    Minimo = str(formatoMuestra % actualMarket["Data"]["Low"]).replace(".", ",")
                    Maximo = str(formatoMuestra % actualMarket["Data"]["High"]).replace(".", ",")
                    Volumen = str(formatoMuestra % actualMarket["Data"]["Volume"]).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'yobit.net'):
                    Compra = str(formatoMuestra % actualMarket["ltc_btc"]["buy"]).replace(".", ",")
                    Venta = str(formatoMuestra % actualMarket["ltc_btc"]["sell"]).replace(".", ",")
                    Minimo = str(formatoMuestra % actualMarket["ltc_btc"]["low"]).replace(".", ",")
                    Maximo = str(formatoMuestra % actualMarket["ltc_btc"]["high"]).replace(".", ",")
                    Volumen = str(formatoMuestra % actualMarket["ltc_btc"]["vol"]).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'livecoin.net'):
                    Compra = str(formatoMuestra % actualMarket["best_bid"]).replace(".", ",")
                    Venta = str(formatoMuestra % actualMarket["best_ask"]).replace(".", ",")
                    Minimo = str(formatoMuestra % actualMarket["low"]).replace(".", ",")
                    Maximo = str(formatoMuestra % actualMarket["high"]).replace(".", ",")
                    Volumen = str(formatoMuestra % actualMarket["volume"]).replace(".", ",")
                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'bitfinex.com'):

                    # valores en BTC
                    marketBid = float(actualMarket["bid"])
                    marketAsk = float(actualMarket["ask"])
                    marketLow = float(actualMarket["low"])
                    marketHigh = float(actualMarket["high"])
                    marketVolume = float(actualMarket["volume"])

                    if(market[0] == 10):
                        # valores en BTC
                        marketBid = marketBid / self.usd_btc_bid
                        marketAsk = marketAsk / self.usd_btc_ask
                        marketLow = marketLow / self.usd_btc_low
                        marketHigh = marketHigh / self.usd_btc_high
                        marketVolume = marketVolume / self.usd_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'bitstamp.net'):
                    # valores en BTC
                    marketBid = float(actualMarket["bid"])
                    marketAsk = float(actualMarket["ask"])
                    marketLow = float(actualMarket["low"])
                    marketHigh = float(actualMarket["high"])
                    marketVolume = float(actualMarket["volume"])

                    if(market[0] == 14):
                        # valores en BTC
                        marketBid = marketBid / self.usd_btc_bid
                        marketAsk = marketAsk / self.usd_btc_ask
                        marketLow = marketLow / self.usd_btc_low
                        marketHigh = marketHigh / self.usd_btc_high
                        marketVolume = marketVolume / self.usd_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])
                
                if(market[2] == 'okcoin.com'):

                    if(market[0] == 21):
                        # obtener los valores en CNY
                        self.cny_btc_bid = float(actualMarket["ticker"]["buy"])
                        self.cny_btc_ask = float(actualMarket["ticker"]["sell"])
                        self.cny_btc_low = float(actualMarket["ticker"]["low"])
                        self.cny_btc_high = float(actualMarket["ticker"]["high"])
                        self.cny_btc_volume = float(actualMarket["ticker"]["vol"])

                    # valores en BTC
                    marketBid = float(actualMarket["ticker"]["buy"])
                    marketAsk = float(actualMarket["ticker"]["sell"])
                    marketLow = float(actualMarket["ticker"]["low"])
                    marketHigh = float(actualMarket["ticker"]["high"])
                    marketVolume = float(actualMarket["ticker"]["vol"])

                    if(market[0] == 15):
                        # valores en BTC
                        marketBid = marketBid / self.usd_btc_bid
                        marketAsk = marketAsk / self.usd_btc_ask
                        marketLow = marketLow / self.usd_btc_low
                        marketHigh = marketHigh / self.usd_btc_high
                        marketVolume = marketVolume / self.usd_btc_bid

                    if(market[0] == 17):
                        # valores en CNY
                        marketBid = marketBid / self.cny_btc_bid
                        marketAsk = marketAsk / self.cny_btc_ask
                        marketLow = marketLow / self.cny_btc_low
                        marketHigh = marketHigh / self.cny_btc_high
                        marketVolume = marketVolume / self.cny_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])
                
                if(market[2] == 'chbtc.com'):
                    # valores en BTC
                    marketBid = float(actualMarket["ticker"]["buy"])
                    marketAsk = float(actualMarket["ticker"]["sell"])
                    marketLow = float(actualMarket["ticker"]["low"])
                    marketHigh = float(actualMarket["ticker"]["high"])
                    marketVolume = float(actualMarket["ticker"]["vol"])

                    if(market[0] == 18):
                        # valores en BTC
                        marketBid = marketBid / self.cny_btc_bid
                        marketAsk = marketAsk / self.cny_btc_ask
                        marketLow = marketLow / self.cny_btc_low
                        marketHigh = marketHigh / self.cny_btc_high
                        marketVolume = marketVolume / self.cny_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])
                
                if(market[2] == 'xbtce.net'):
                    # valores en BTC
                    marketBid = float(actualMarket[0]["BestBid"])
                    marketAsk = float(actualMarket[0]["BestAsk"])
                    marketLow = float(actualMarket[0]["DailyBestBuyPrice"])
                    marketHigh = float(actualMarket[0]["DailyBestSellPrice"])
                    marketVolume = float(actualMarket[0]["DailyTradedTotalVolume"])

                    if(market[0] == 13):
                        # valores en BTC
                        marketBid = marketBid / self.usd_btc_bid
                        marketAsk = marketAsk / self.usd_btc_ask
                        marketLow = marketLow / self.usd_btc_low
                        marketHigh = marketHigh / self.usd_btc_high
                        marketVolume = marketVolume / self.usd_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])

                if(market[2] == 'bithumb.com'):

                    if(market[0] == 20):
                        # obtener los valores en KRW
                        self.krw_btc_bid = float(actualMarket["data"]["buy_price"])
                        self.krw_btc_ask = float(actualMarket["data"]["sell_price"])
                        self.krw_btc_low = float(actualMarket["data"]["min_price"])
                        self.krw_btc_high = float(actualMarket["data"]["max_price"])
                        self.krw_btc_volume = float(actualMarket["data"]["volume_1day"])

                    # valores en BTC
                    marketBid = float(actualMarket["data"]["buy_price"])
                    marketAsk = float(actualMarket["data"]["sell_price"])
                    marketLow = float(actualMarket["data"]["min_price"])
                    marketHigh = float(actualMarket["data"]["max_price"])
                    marketVolume = float(actualMarket["data"]["volume_1day"])

                    if(market[0] == 19):
                        # valores en BTC
                        marketBid = marketBid / self.krw_btc_bid
                        marketAsk = marketAsk / self.krw_btc_ask
                        marketLow = marketLow / self.krw_btc_low
                        marketHigh = marketHigh / self.krw_btc_high
                        marketVolume = marketVolume / self.krw_btc_bid

                    Compra = str(formatoMuestra % marketBid).replace(".", ",")
                    Venta = str(formatoMuestra % marketAsk).replace(".", ",")
                    Minimo = str(formatoMuestra % marketLow).replace(".", ",")
                    Maximo = str(formatoMuestra % marketHigh).replace(".", ",")
                    Volumen = str(formatoMuestra % marketVolume).replace(".", ",")

                    timestamp = datetime.datetime.fromtimestamp(time.time()).strftime('%Y-%m-%d %H:%M:%S')
                    self.toSaveData.append([market[2], Compra, Venta, Minimo, Maximo, Volumen, timestamp, market[0]])
            
            except KeyError, e:
                print '********* Problema con la API de: '+market[2]+' (Verificar) *********'
            except Exception, e:
                print '**Algun problema con: ' + market[2]

            time.sleep(1.5)

            try:
                print ("%s: (%0.2f seg)" % (market[2], (time.time() - start_time)))
            except Exception:
                print ("%s: (%s)" % (market[2], "Tiempo desconocido"))
            start_time = time.time()

# Main
# ---
# -

Values = CrypTValues()

# numero de hilos, todos los mercados a la vez
pool = ThreadPool(35) 

#inicializar la base de datos
Values.iniMarkets()

t = threading.Thread(target=Values.updateAll)
t.start()

# obtener todos los resultados a la vez y seguirlos obteniendo con
#    un tiempo de espera de llamada menor a un segundo
pool.map(Values.getMarket, Values.markets)

