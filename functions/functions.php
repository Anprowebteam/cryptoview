<?php 

date_default_timezone_set('America/New_York');
require_once 'conection.php';

function getCryptoData()
{
	$DB = new Conexion();

	$stringQuery = "SELECT * FROM data";

	$Query = $DB->prepare($stringQuery);
	$Query->execute();

	$response = "";
		
	if($Query->rowCount())
	{
		while ($Mercado = $Query->fetch()) {
			
			$response .= "<tr>";

			$Mercado_ = $Mercado["market"];
			$Change = $Mercado["change_"];
			$Compra = $Mercado["bid"];
			$Venta = $Mercado["ask"];
			$Minimo = $Mercado["low"];
			$Maximo = $Mercado["high"];
			$Volumen = $Mercado["volume"];
			$LastUpdated = $Mercado["update_time"];

			$segundos = time() - strtotime($LastUpdated);

			$response .= "<th class='mercado'>$Mercado_ <small>($Change)</small)</th>
			<td>$Compra</td>
			<td>$Venta</td>
			<td>$Minimo</td>
			<td>$Maximo</td>
			<td>$Volumen</td>
			<td class='opaco'>$segundos s.</td>";

	  		$response .= "</tr>";
		}
	}

	return $response;
}
